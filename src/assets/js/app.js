$(document).foundation();

//Navigation active element border-bottom
 // $("ul .dropdown li a").click(function() {
 //    // remove classes from all
 //    //$("ul .dropdown li a").removeClass("inactive");
 //    // add class to the one we clicked
 //    $(this).addClass("active");
 // });

//navigation off-canvas temp-fix
$('.js-off-canvas-exit').hide();

// Parallax effects

$(".parallax-scroll").each(function(){
  $(window).scroll(function(){
    //Variable for seeing window is scrolling
    var wScroll = $(this).scrollTop();
      //If window is scrolled past 10% then add fly-in-tiles class for tiles
      if(wScroll > $('.move-up').offset().top - ($(window).height() / 1.2)) {
        $('.move-up .thumbnail').each(function(){
          $('.thumbnail').addClass('fly-in-tiles');
        });
      }
      //If window is scrolled past 20% then add class is-showing for intro blub
      if(wScroll > $('.blurb').offset().top - ($(window).height() / 1.2)) {
        $('.blurb').addClass('is-showing');
      }
    });
});

//top-bar shrinky dinky
$(document).on("scroll",function(){
    if($(document).scrollTop()>100){
        $(".top-bar").addClass("smallBar");
        $(".menu-text").removeClass("logo").addClass("smallLogo");
    } else{
        $(".top-bar").removeClass("smallBar")
        $(".menu-text").addClass("logo").removeClass("smallLogo");
    }
});



//Show hide services list
$(function() {
  $(".expand").on( "click", function() {
    $(this).next().slideToggle(100);
    $expand = $(this).find(">:first-child");

    if($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
});

//Technology
$(function() {
   $(".subnav-nav a").click(function() {
     // remove classes from all
     $(".subnav-nav a").removeClass("is-active");
     // add class to the one we clicked
     $(this).addClass("is-active");
   });
});

//show hide tech sections
//For main technoology page hooks
$('.tech a').on('click', function() {
  $(".tech").hide();
  $('.techsection').removeClass('show-section');
  var id = $(this).data('id'),
    $target = $('section[data-id="' + id + '"]');
  $target.toggle('show-section');
});

//For the subnav hooks
$('.subnav-nav li a').on('click', function() {
  $(".tech").hide();
  $('.techsection').removeAttr('style');
  $('.techsection').removeClass('show-section');
  var id = $(this).data('id'),
    $target = $('section[data-id="' + id + '"]');
  $target.addClass('show-section');
});


//Slideshow using owl-carousel
$("#owl-demo").owlCarousel({
  navigation : true, // Show next and prev buttons
  slideSpeed : 300,
  paginationSpeed : 400,
  singleItem:true
});

//Publications odd color change
$(".publications-list .media-object:even").css('background-color', '#ADC2C3');

//Contact Form Send
$("#submit").click(function() {
var name = $("#name").val();
var email = $("#email").val();
var message = $("#message").val();
var contact = $("#contact").val();
$("#returnmessage").empty(); // To empty previous error/success message.
// Checking for blank fields.
if (name == '' || email == '' || contact == '') {
alert("Please Fill Required Fields");
} else {
// Returns successful data submission message when the entered information is stored in database.
$.post("contact_form.php", {
name1: name,
email1: email,
message1: message,
contact1: contact
}, function(data) {
$("#returnmessage").append(data); // Append returned message to message paragraph.
if (data == "Thank you, your message has been received and will contact you soon.") {
$("#form")[0].reset(); // To reset form fields on success.
}
});
}
});


//Read more expand
// Configure/customize these variables.
    var showChar = 400;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";
    $('.more').each(function() {
        var content = $(this).html();
        if(content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
            $(this).html(html);
        }
    });
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });




//Resume Form Send
$("#submit_btn").click(function() {
  console.log("After Submit");

	    var proceed = true;
        //simple validation at client's end
        //loop through each field and we simply change border color to red for invalid fields
		$("#contact_form input[required=true], #contact_form textarea[required=true]").each(function(){
			$(this).css('border-color','');
			if(!$.trim($(this).val())){ //if this field is empty
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
			//check invalid email
			var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if($(this).attr("type")=="email" && !email_reg.test($.trim($(this).val()))){
				$(this).css('border-color','red'); //change border color to red
				proceed = false; //set do not proceed flag
			}
		});
    console.log("Prior to proceed"+ proceed);

        if(proceed) //everything looks good! proceed...
        {
           //data to be sent to server
            var m_data = new FormData();
            m_data.append( 'user_name', $('input[name=name]').val());
            m_data.append( 'user_email', $('input[name=email]').val());
            m_data.append( 'country_code', $('input[name=phone1]').val());
            m_data.append( 'phone_number', $('input[name=phone2]').val());
            m_data.append( 'subject', $('select[name=subject]').val());
			m_data.append( 'msg', $('textarea[name=message]').val());
			m_data.append( 'file_attach', $('input[name=file_attach]')[0].files[0]);

      console.log("Before Ajax");


            //instead of $.post() we are using $.ajax()
            //that's because $.ajax() has more options and flexibly.
  			$.ajax({
              url: '../../../contact_me.php',
              data: m_data,
              processData: false,
              contentType: false,
              type: 'POST',
              dataType:'json',
              success: function(response){
                 //load json data from server and output message
 				if(response.type == 'error'){ //load json data from server and output message
					output = '<div class="error">'+response.text+'</div>';
				}else{
				    output = '<div class="success">'+response.text+'</div>';
				}
				$("#contact_form #contact_results").hide().html(output).slideDown();
              }
            });
            console.log("After Ajax");



        }
    });

    //reset previously set border colors and hide all message on .keyup()
    $("#contact_form  input[required=true], #contact_form textarea[required=true]").keyup(function() {
        $(this).css('border-color','');
        $("#result").slideUp();
    });
